# qt-appshot
An instant screenshots generator with custom labels and captions (C++ Qt5)

It can generate pixmap screenshot of any widget (incl. main window) with labels and captions on-the fly from any widget data (a QProperty or Tooltip for example, but this can be customized by subclassing AppShot class).

Easy to use: just include header and source files from /src into Your C++ Qt5 project

Examlpe: tooltips used as label text source
![ScreenShot](screenshot1.jpeg)

Examlpe: QProperty "objectName" used as label text source
![ScreenShot](screenshot2.jpeg)

Examples are included in test folder. Build it with Qt5 and see how it works.

License: MIT

v1.0
