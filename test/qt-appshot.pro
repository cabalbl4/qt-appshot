#-------------------------------------------------
#
# Project created by QtCreator 2015-12-13T16:49:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt-appshot
TEMPLATE = app


SOURCES += ../src/appshot.cpp\
        main.cpp\
        maintestwindow.cpp


HEADERS  += ../src/appshot.h \
        maintestwindow.h


FORMS    += maintestwindow.ui
