#include "maintestwindow.h"
#include "ui_maintestwindow.h"

MainTestWindow::MainTestWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainTestWindow)
{
    ui->setupUi(this);
    ui->dial->setProperty("testProperty", tr("I am a dial"));
    ui->groupBox->setProperty("testProperty", tr("ChangeMe"));
}

MainTestWindow::~MainTestWindow()
{


    delete ui;
}
