#ifndef MAINTESTWINDOW_H
#define MAINTESTWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainTestWindow;
}

class MainTestWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainTestWindow(QWidget *parent = 0);
    ~MainTestWindow();

private:
    Ui::MainTestWindow *ui;
};

#endif // MAINTESTWINDOW_H
