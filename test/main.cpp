#include "maintestwindow.h"
#include <QApplication>
#include <QLabel>
#include <QPen>
#include <QTextOption>

#include "../src/appshot.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainTestWindow w;
    w.show();







    QTextOption l_opt;
    l_opt.setAlignment(Qt::AlignCenter);

    QLabel myLabel1;
    myLabel1.setPixmap(APPSHOT::AppShot::generateImageFromQProperty( &w, "objectName", l_opt ));

    myLabel1.show();

    QLabel myLabel2;
    myLabel2.setPixmap(APPSHOT::AppShot::generateImageFromQProperty( &w, "testProperty" ));

    myLabel2.show();



    QLabel myLabel;

    APPSHOT::AppShotToolTip t;
    QPen l_Pen;
    l_Pen.setWidth(2);
    l_Pen.setColor(QColor("grey"));
    l_Pen.setStyle(Qt::DashDotDotLine);
    t.setPen(l_Pen);
    t.setTextPen(l_Pen);
    myLabel.setPixmap(t.generate(&w));

    myLabel.show();


    return a.exec();
}
