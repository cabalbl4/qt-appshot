#include "appshot.h"

#include <algorithm>
#include <cmath>
#include <QDateTime>
#include <QListIterator>
#include <QRect>
#include <QLabel>
#include <QPainter>
#include <QXmlStreamReader>

namespace APPSHOT {



AppShot::AppShot(Options _options, QTextOption _textOption, QList<QValidator *> _validators) :
    m_options(_options),
    m_textOption(_textOption),
    m_validators(_validators)
{
    m_linePen.setBrush( QBrush( Qt::SolidLine ));
    m_linePen.setColor( QColor("black") );
    m_linePen.setStyle(Qt::SolidLine);
    m_linePen.setWidth(1);
    m_rectPen = m_linePen;
    m_rectPen.setWidth(2);
    m_rectPen.setStyle(Qt::DotLine);
    m_minRect.setWidth(50);
    m_minRect.setHeight(10);
    m_Field = 20;
}
QPen AppShot::textPen() const
{
    return m_textPen;
}

void AppShot::setTextPen(const QPen &textPen)
{
    m_textPen = textPen;
}

QRect AppShot::minRect() const
{
    return m_minRect;
}

void AppShot::setMinRect(const QRect &minRect)
{
    m_minRect = minRect;
}

QPen AppShot::rectPen() const
{
    return m_rectPen;
}

void AppShot::setRectPen(const QPen &rectPen)
{
    m_rectPen = rectPen;
}

QPen AppShot::pen() const
{
    return m_linePen;
}

void AppShot::setPen(const QPen &pen)
{
    m_linePen = pen;
}

QList<QValidator *> AppShot::validators() const
{
    return m_validators;
}

void AppShot::setValidators(const QList<QValidator *> &validators)
{
    m_validators = validators;
}

QTextOption AppShot::textOption() const
{
    return m_textOption;
}

void AppShot::setTextOption(const QTextOption &textOption)
{
    m_textOption = textOption;
}

QPixmap AppShot::generateImageFromToolTips(QWidget *_target, QTextOption _textOption, AppShot::Options _options, QList<QValidator *> _validators)
{
    AppShotToolTip l_a(_options,_textOption,_validators);
    return l_a.generate(_target);
}

QPixmap AppShot::generateImageFromQProperty(QWidget *_target, const QString &_propName, QTextOption _textOption, AppShot::Options _options, QList<QValidator *> _validators)
{
    AppShotQProperty l_a(_propName, _options,_textOption,_validators);
    return l_a.generate(_target);
}

QPixmap AppShot::generate(QWidget *_target)
{
    if(! _target) return QPixmap();
    QList<AppShot::ScreenEntery> l_data;
    walk(_target, l_data);


    return paintData(l_data, _target);

}

AppShot::Options AppShot::options() const
{
    return m_options;
}

void AppShot::setOptions(const  AppShot::Options &options)
{
    m_options = options;
}


void AppShot::walk(QWidget *_widget, QList<AppShot::ScreenEntery> &_data)
{
    if(! _widget) return;
    if(! ( m_options & IncludeHidden ) ) {
      if(! _widget->isVisible() ) return;
    };



    AppShot::ScreenEntery l_ent;

    l_ent.screenRect = _widget->geometry();
    QPoint p(0,0);
    l_ent.screenRect.moveTop(  _widget->mapToGlobal(p).y() );
    l_ent.screenRect.moveLeft(  _widget->mapToGlobal(p).x() );

    QString l_name = getWidgetDescriptiveText(_widget);
    l_ent.text = l_name;

    for(int i=0; i<m_validators.length(); i++) {
      int a = 0;
      if(m_validators.at(i)->validate(l_name,a) != QValidator::Acceptable) return;
    };

    if(! l_name.isEmpty()) _data.append(l_ent);

    foreach(QObject * obj, _widget->children()) {
        walk(dynamic_cast<QWidget*>(obj), _data);
    };



}

QPixmap AppShot::paintData(QList<AppShot::ScreenEntery> _data, QWidget * _mainWidget)
{
    int l_spacer = m_Field;
    typedef QPair<SIDE, AppShot::ScreenEntery> sidelist;
    QList<sidelist> l_sides;

    /// global coords to local
    QPoint pt(0,0);
    QRect l_widgetRect = _mainWidget->geometry();
    l_widgetRect.moveTop(  _mainWidget->mapToGlobal(pt).y() );
    l_widgetRect.moveLeft(  _mainWidget->mapToGlobal(pt).x() );
    int l_minusX = l_widgetRect.x();
    int l_minusY = l_widgetRect.y();

    l_widgetRect.moveTop(  l_widgetRect.y() - l_minusY );
    l_widgetRect.moveLeft(  l_widgetRect.x() - l_minusX );

    for(int i=0;i<_data.length();i++){
       _data[i].screenRect.moveTop( _data[i].screenRect.y() - l_minusY );
       _data[i].screenRect.moveLeft( _data[i].screenRect.x() - l_minusX );
    };


    foreach(AppShot::ScreenEntery ent, _data) {
      l_sides.append(qMakePair(
                         getNearestEdge(ent.screenRect, _mainWidget->rect()),
                         ent
                         ));
    };

    QList<QRectF> l_captionRects;


    for(int i=0; i < l_sides.length(); i++  ) {
      QString l_Text = l_sides.at(i).second.text;
      QPixmap l_t(_mainWidget->rect().width()*3, _mainWidget->rect().height()*3);
      QPainter p(&l_t);
      QRectF l_rect = p.boundingRect(m_minRect,l_Text, m_textOption);

      l_rect.moveCenter( l_sides.at(i).second.screenRect.center()  );
      if( l_sides.at(i).first == LEFT ) {
         l_rect.moveRight(0);
      } else if( l_sides.at(i).first == RIGHT ) {
          l_rect.moveLeft(l_widgetRect.width());
      } else if( l_sides.at(i).first == TOP ) {
          l_rect.moveBottom(0);
      } else {
         l_rect.moveTop(l_widgetRect.height());
      }

      // Do not allow captions to intersect
      bool intersect = true;
      int l_bounce = 0;
      int l_secondBounce = 0;
      QRectF l_initial = l_rect;
      while (intersect) {
        intersect =false;
        foreach(QRectF r, l_captionRects){
          if(r.intersects( l_rect.toRect() )){
              intersect = true;
              break;
          }

        };

        if(intersect) {
          l_bounce = bounce(l_bounce);
          if(( l_sides.at(i).first == LEFT ) ||
              ( l_sides.at(i).first == RIGHT ))  {
             l_rect.moveTop( l_rect.top() + l_bounce );
             if( abs(l_bounce) > _mainWidget->rect().width() /2 ) {
                 l_rect.moveTop(l_initial.y());
                 l_bounce = 0;
                 l_secondBounce++;
                 if(l_sides.at(i).first == LEFT) {
                   l_rect.moveLeft( l_rect.x() - l_secondBounce);
                 } else {
                   l_rect.moveLeft( l_rect.x() + l_secondBounce);
                 }
             }
          } else {
              l_rect.moveLeft( l_rect.left() + l_bounce );
              if( abs(l_bounce) > _mainWidget->rect().height() /2 ) {
                  l_rect.moveLeft( l_initial.x() );
                  l_bounce = 0;
                  l_secondBounce ++;
                  if(l_sides.at(i).first == TOP) {
                    l_rect.moveTop( l_rect.y() - l_secondBounce);
                  } else {
                    l_rect.moveTop( l_rect.y() + l_secondBounce);
                  }
              }

          }



        };


      };
      l_captionRects.append(l_rect);


    };


    int l_width = 0;
    int l_height = 0;
    int l_leftOffset = 0;
    int l_topOffset = 0;
    int l_minimumX =0;
    int l_minimumY =0;

    for(int i=0; i<l_captionRects.length(); i++) {
        if(l_captionRects.at(i).toRect().x() < l_minimumX) l_minimumX = l_captionRects.at(i).toRect().x();
        if(l_captionRects.at(i).toRect().y() < l_minimumY) l_minimumY = l_captionRects.at(i).toRect().y();
        if(l_captionRects.at(i).toRect().x() < 0) {
            if( abs(l_captionRects.at(i).toRect().x()) > l_leftOffset ) l_leftOffset =  abs(l_captionRects.at(i).toRect().x()) ;
        }
        if(l_captionRects.at(i).toRect().y() < 0) {
            if( abs(l_captionRects.at(i).toRect().y()) > l_topOffset ) l_topOffset =  abs(l_captionRects.at(i).toRect().y()) ;
        }

        if((l_captionRects.at(i).toRect().x() + l_captionRects.at(i).toRect().width()) > l_width)
            l_width = l_captionRects.at(i).toRect().x() + l_captionRects.at(i).toRect().width();
        if((l_captionRects.at(i).toRect().y() + l_captionRects.at(i).toRect().height()) > l_height)
            l_height = l_captionRects.at(i).toRect().y() + l_captionRects.at(i).toRect().height();

    };

    l_leftOffset += l_spacer;
    l_topOffset += l_spacer;
    l_width += l_leftOffset;
    l_height += l_topOffset;
    if(l_width < (l_leftOffset + l_widgetRect.width()))
        l_width = (l_leftOffset + l_widgetRect.width());
    if(l_height < l_topOffset + l_widgetRect.height()) {
        l_height =   (l_topOffset + l_widgetRect.height());
    };

    l_width += l_spacer * 2;
    l_height += l_spacer *2;

   for(int i=0; i<l_captionRects.length(); i++) {

      if( l_sides.at(i).first == LEFT ) {
          l_captionRects[i].moveLeft( l_captionRects[i].x() + l_leftOffset - l_spacer);
          l_captionRects[i].moveTop( l_captionRects[i].y() + l_topOffset );
      } else if( l_sides.at(i).first == RIGHT ) {
          l_captionRects[i].moveLeft( l_captionRects[i].x() + l_leftOffset + l_spacer);
          l_captionRects[i].moveTop( l_captionRects[i].y() + l_topOffset );
      } else if( l_sides.at(i).first == TOP ) {
          l_captionRects[i].moveLeft( l_captionRects[i].x() + l_leftOffset );
          l_captionRects[i].moveTop( l_captionRects[i].y() + l_topOffset - l_spacer);
      } else {
          l_captionRects[i].moveLeft( l_captionRects[i].x() + l_leftOffset );
          l_captionRects[i].moveTop( l_captionRects[i].y() + l_topOffset + l_spacer);
      }



    };



    QPixmap l_result( l_width, l_height );
    l_result.fill();



    QPainter p(&l_result);
    p.save();
    p.drawPixmap(l_leftOffset, l_topOffset, _mainWidget->grab());
    p.restore();

    for(int i=0; i<l_captionRects.length(); i++) {
      p.save();
      p.setPen(m_textPen);
      p.drawText( l_captionRects.at(i),  l_sides.at(i).second.text, m_textOption);
      p.setPen(m_linePen);

      QRect l_sr = l_sides.at(i).second.screenRect;
      l_sr.moveLeft( l_sr.x()+ l_leftOffset);
       l_sr.moveTop(l_sr.y() + l_topOffset);


       int l_xExtend = 0;
       int l_yExtend = 0;

       int l_txExtend =0;
       int l_tyExtend =0;

       if( l_sides.at(i).first == LEFT ) {
          l_xExtend += l_captionRects.at(i).width() /2;
          l_txExtend += -l_sr.width()/2;
       } else if( l_sides.at(i).first == RIGHT ) {
          l_xExtend +=  -l_captionRects.at(i).width() /2;
           l_txExtend += l_sr.width()/2;
       } else if( l_sides.at(i).first == TOP ) {
           l_yExtend += l_captionRects.at(i).height() /2;
            l_tyExtend += -l_sr.height()/2;
       } else {
          l_yExtend += -l_captionRects.at(i).height() /2;
          l_tyExtend += l_sr.height()/2;
       }


      p.drawLine( l_captionRects.at(i).center().x() + l_xExtend,
                  l_captionRects.at(i).center().y() + l_yExtend,
                  l_sr.center().x() + l_txExtend,
                  l_sr.center().y() + l_tyExtend
                  );
      p.setPen(m_rectPen);
      p.drawRect(l_sr);
      p.restore();
    };

   return l_result;



}

AppShot::SIDE AppShot::getNearestEdge(QRect _child, QRect _parent)
{
    QList<int> l_seeds;
    l_seeds.append( _child.topLeft().y() - _parent.topLeft().y()  );
    l_seeds.append(   _parent.bottomRight().y()  - _child.bottomRight().y());
    l_seeds.append( _child.topLeft().x() - _parent.topLeft().x() );
    l_seeds.append( _parent.topRight().x() - _child.topRight().x());
    QListIterator<int> it(l_seeds);
    QList<int>::iterator l_min = std::min_element(l_seeds.begin(), l_seeds.end());
    int l_pos = std::distance(l_seeds.begin(), l_min);

        switch (l_pos) {
        case 0:
            return TOP;
            break;
        case 1:
            return BOTTOM;
            break;
        case 2:
            return LEFT;
            break;
        case 3:
            return RIGHT;
            break;
        default:
            throw -1;
            break;
        }




}

int AppShot::bounce(int px)
{
    if(px ==0) return 1;
    if(px >0) return -px;
    return abs(px) + 35;
}
int AppShot::field() const
{
    return m_Field;
}

void AppShot::setField(int Field)
{
    m_Field = Field;
}


AppShotToolTip::AppShotToolTip(AppShot::Options _options,
                               QTextOption _textOption,
                                 QList<QValidator *> _validators)
    : AppShot(_options, _textOption,_validators)
{
    // Nothing
}

QString AppShotToolTip::getWidgetDescriptiveText(QWidget *_target)
{
    //return _target->toolTip();

    QXmlStreamReader xml(_target->toolTip());
    QString l_textString;
    bool l_read = false;
    while (!xml.atEnd()) {
        if ( xml.readNext() == QXmlStreamReader::Characters ) {
            l_textString += xml.text();
            l_read = true;
            if(xml.error() != QXmlStreamReader::NoError)
                return _target->toolTip();
        }
    }
    if(! l_read) return _target->toolTip();
    return l_textString;

}

AppShotQProperty::AppShotQProperty(QString _propName, Options _options,
                                     QTextOption _textOption,
                                     QList<QValidator *> _validators)
    : AppShot(_options, _textOption,_validators),
      m_propName(_propName)
{
    //Nothing
}

QString AppShotQProperty::getWidgetDescriptiveText(QWidget *_target)
{
  return _target->property(m_propName.toStdString().c_str()).toString();
}



}// namespace APPSHOT

