#ifndef APPSHOT_H
#define APPSHOT_H

#include <QPixmap>
#include <QWidget>
#include <QList>
#include <QValidator>
#include <QRect>
#include <QString>
#include <QTextOption>
#include <QPen>

// Author: Vokhmin Ivan cabalbl4@gmail.com

namespace APPSHOT {



/** @class AppShot
 * Abstract class to create shots.
 *
 */
class AppShot
{
public: 
    virtual ~AppShot() {}
    enum Options {
        DefaultOptions=0,
        IncludeHidden=1
    };
    enum SIDE {
                TOP,
                BOTTOM,
                LEFT,
                RIGHT
            };

    /// Inner structure. @fn walk(QWidget * _widget, QList<ScreenEntery> & _data) generates these
    struct ScreenEntery {
        QRect screenRect;
        QString text;
    };

    /// Static function to generate pixmap from tooltips
    static QPixmap generateImageFromToolTips(QWidget * _target,
                                            QTextOption _textOption = QTextOption(),
                                            AppShot::Options _options = AppShot::DefaultOptions,
                                            QList<QValidator*> _validators = QList<QValidator*>()
            );
    /// Static function to generate pixmap from QProperties
    static QPixmap generateImageFromQProperty(QWidget * _target,
                                             const QString & _propName,
                                             QTextOption _textOption = QTextOption(),
                                             AppShot::Options _options = AppShot::DefaultOptions,
                                             QList<QValidator*> _validators = QList<QValidator*>()
            );

    virtual QPixmap generate(QWidget * _target);



    Options options() const;
    void setOptions(const Options &options);

    /// Text options for labels
    QTextOption textOption() const;
    /// Set text options for labels
    void setTextOption(const QTextOption &textOption);
    QList<QValidator *> validators() const;
    /// Set validators. If a label text will not pass any of them - it will not be displayed
    void setValidators(const QList<QValidator *> &validators);
    QPen pen() const;
    /// Set pen to draw lines between rect and label
    void setPen(const QPen &pen);
    QPen rectPen() const;
     /// Set pen to draw rects
    void setRectPen(const QPen &rectPen);
    QRect minRect() const;
    /// Set minimum rect for a label
    void setMinRect(const QRect &minRect);
    QPen textPen() const;
    ///Set text to draw text
    void setTextPen(const QPen &textPen);
    int field() const;
    /// Field in px between screenshot in labels. Defaults to 20
    void setField(int field);

protected:
    AppShot(Options _options,
            QTextOption _textOption = QTextOption(),
            QList<QValidator*> _validators = QList<QValidator*>()
            );
    /// Screen widget walker
    virtual void walk(QWidget * _widget, QList<ScreenEntery> & _data);
    /// A function to get widgets description. Should be overriden by inheritance
    virtual QString getWidgetDescriptiveText(QWidget *target)= 0;
    /// A function to create pixmap and draw all captions
    virtual QPixmap paintData(QList<ScreenEntery> _data, QWidget *_mainWidget);
    /// Find nearest edge to paint label.
    virtual SIDE getNearestEdge(QRect _child, QRect _parent);
    /// A simple function to move labels if they intersect
    int bounce(int px);


    Options m_options;
    QTextOption m_textOption;
    QList<QValidator*> m_validators;
    /// Pen to draw lines
    QPen m_linePen;
    /// Minimum rect. A label will take this size at minimum
    QRect m_minRect;
    /// Pen to draw rects
    QPen m_rectPen;
    QPen m_textPen;
    int m_Field;


private:
    AppShot(){}
};

/// Class to create appshots from tooltips
class AppShotToolTip : public AppShot {
public:
    AppShotToolTip(Options _options = AppShot::DefaultOptions,
                    QTextOption _textOption = QTextOption(),
                    QList<QValidator*> _validators = QList<QValidator*>()
                    );
    virtual ~AppShotToolTip(){}
protected:
    virtual QString getWidgetDescriptiveText(QWidget *_target);

};

/// Class to create appshots from QProperties
class AppShotQProperty : public AppShot{
public:
    AppShotQProperty(
                    QString _propName,
                    Options _options = AppShot::DefaultOptions,
                    QTextOption _textOption = QTextOption(),
                    QList<QValidator*> _validators = QList<QValidator*>()
                    );
    virtual ~AppShotQProperty(){}
protected:
    virtual QString getWidgetDescriptiveText(QWidget *_target);

    QString m_propName;

};

} //namespace _APPSHOT

#endif // APPSHOT_H
